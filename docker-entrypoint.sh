#!/bin/bash
set -e

/bin/bash -l -c "bower install --allow-root"
cp -r src/* /usr/share/nginx/html

if [[ "$SERVER_NAME" ]]; then
    sed -i "s|\$SERVER_NAME|$SERVER_NAME|g" /etc/nginx/conf.d/default.conf
fi

echo
echo "Starting nginx"
echo

exec "$@"
