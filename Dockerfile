FROM nginx:1.11.1

MAINTAINER Thiago Henrique Poiani <thpoiani@gmail.com>
WORKDIR /portal

RUN apt-get update && apt-get install -y \
  git curl \
  && rm -rf /var/lib/apt/lists/*

RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
RUN /bin/bash -l -c "nvm install node"

RUN /bin/bash -l -c "npm install bower -g"

COPY bower.json /portal/bower.json
COPY .bowerrc /portal/.bowerrc
COPY src /portal/src

VOLUME /portal/src

COPY default.conf /etc/nginx/conf.d/default.conf

COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 80
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]
