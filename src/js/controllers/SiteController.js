(function() {

	'use strict';

	angular.module('webApp').controller('SiteController', SiteController);

	function SiteController(SearchResource, $routeParams, $rootScope, $location, $log){

		var sc = this;

		$log.debug('$routeParams',$routeParams)

        $routeParams.$watch('id', function(old, current, $scope) {
            if (old && !current) {
            	sc.getResults($routeParams.id);
            }
        });

		sc.doSearch = function(){
			$log.debug('search', sc.search)
			$location.path('/search/' + sc.search)
		}

		sc.getResults = function(query){
			SearchResource.get({ searchString:query }, function(searchRes) {
				$log.debug('searchRes', searchRes)
				sc.searchResponse = searchRes.data;
			}, function(err) {
				$log.debug('error', err)
			})
		}

	}

})();