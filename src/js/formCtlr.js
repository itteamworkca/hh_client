'use strict';

angular.module('ciebform').controller('formCtrl', formCtrl);

function formCtrl($rootScope, $http, $location, $log, $window, $compile, $q, $scope, FileUploader){
	
	$scope.entity = {};
	$scope.currentStep = 1;
	$scope.listaEmpreendedores = [];
	$scope.listaClassificacoes = [];
	$scope.listaClientes = [];
	$scope.clientesProduto = [];
	var uploader = $scope.uploader = new FileUploader();

	var endpoint = "url/url"

	$http.get('json/form.json').then(function(res) {
		$scope.formItems = res.data;
		return res;
	});

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
	

	$scope.toggleView = function(view){
		view = view.split('.');
		return $scope.entity[view[0]][view[1]][0];
	}

	$scope.stepnav = function(step){
		$scope.currentStep = step;
	}

	$scope.getModel = function(path) {
	  var segs = path.split('.');
	  var root = $scope.entity;

	  while (segs.length > 0) {
	    var pathStep = segs.shift();
	    if (typeof root[pathStep] === 'undefined') {
	      root[pathStep] = segs.length === 0 ? [ '' ] : {}    
	    }
	    root = root[pathStep];
	  }
	  return root;
	}
	 
	function getProps(obj){
	  var objToExport = {};
	  for (var property in obj) {
	    if (obj.hasOwnProperty(property)){
	        if (obj[property].constructor == Object) {
	            objToExport[property] = getProps(obj[property]);
	        } else {
	            objToExport[property] = obj[property][0]&&obj[property][0]!=''&&obj[property][0]!=null ? obj[property][0] : null;
	        }
	    }
	  }
	  return objToExport;
	}

	$scope.formButtonClick = function(action){
		switch(action) {
		    case "cadastrarEmpreendedor":
		        $scope.cadastrarEmpreendedor();
		        break;
		}
	}

	$scope.cadastrarEmpreendedor = function(){
		var emps = angular.copy($scope.entity.empreendedor)
		var empsF = getProps(emps);
		$scope.listaEmpreendedores.push(empsF);
		for (var k in $scope.entity.empreendedor) {
			if ({}.hasOwnProperty.call($scope.entity.empreendedor, k)) {
				$scope.entity.empreendedor[k][0]='';
			}
		}
	}

	$scope.cadastrarClassificacao = function(){
		var clas = angular.copy($scope.entity.produto.classificacao)
		var clasF = getProps(clas);
		$scope.listaClassificacoes.push(clasF);
		for (var k in $scope.entity.produto.classificacao) {
			if ({}.hasOwnProperty.call($scope.entity.produto.classificacao, k)) {
				$scope.entity.produto.classificacao[k][0]='';
			}
		}
	}

	$scope.cadastrarCliente = function(){
		var cli = angular.copy($scope.entity.empresa.principaisClientes)
		var cliF = getProps(cli);
		$scope.listaClientes.push(cliF);
		for (var k in $scope.entity.empresa.principaisClientes) {
			if ({}.hasOwnProperty.call($scope.entity.empresa.principaisClientes, k)) {
				$scope.entity.empresa.principaisClientes[k][0]='';
			}
		}
	}

	$scope.cadastrarClienteProduto = function(){
		var ccp = angular.copy($scope.entity.produto.principaisClientes)
		var ccpF = getProps(ccp);
		$scope.clientesProduto.push(ccpF);
		for (var k in $scope.entity.produto.principaisClientes) {
			if ({}.hasOwnProperty.call($scope.entity.produto.principaisClientes, k)) {
				$scope.entity.produto.principaisClientes[k][0]='';
			}
		}
	}

	$scope.removeItemFromList = function(item){
		$scope.listaEmpreendedores.splice($scope.listaEmpreendedores.indexOf(item), 1);
	}

	$scope.removeClassificacao = function(item){
		$scope.listaClassificacoes.splice($scope.listaClassificacoes.indexOf(item), 1);
	}

	$scope.removeCliente = function(item){
		$scope.listaClientes.splice($scope.listaClientes.indexOf(item), 1);
	}

	$scope.removeClienteProduto = function(item){
		$scope.clientesProduto.splice($scope.clientesProduto.indexOf(item), 1);
	}

	$scope.formPost = function(){
		$log.debug('$scope.entity', $scope.entity);
		var obj = angular.copy($scope.entity);
		var toPost = getProps(obj);
		toPost["empreendedor"] = $scope.listaEmpreendedores;
		toPost["empresa"]["principaisClientes"] = $scope.listaClientes;
		toPost["produto"]["tags"] = $scope.listaClassificacoes;
		toPost["produto"]["principaisClientes"] = $scope.clientesProduto;
		$log.debug('toPost', toPost);

        $http.post(endpoint, toPost)
            .then(function(response) {
            	$scope.currentStep = 4;
            	$log.debug('deu bom!', response);
            }, function(response) {
            	$scope.currentStep = 5;
            	$log.debug('deu ruim!', response);
        	});

	}

	$log.info($scope)

}