(function(window, document, angular, moment, $, undefined) {
    'use strict';

    angular
        .module('webApp', [
            'ngResource',
            'ngRoute',
            'ngSanitize'
        ])

    .config(function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/home.html'
            })
            .when('/search/:id', {
                templateUrl: 'views/search.html'
            })
            .otherwise({
                templateUrl: '/404.html'
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    })

    .config(function($logProvider) {
        $logProvider.debugEnabled(true);
    })

    .config(function($httpProvider) {
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        $httpProvider.defaults.transformRequest = function(data) {
            if (data === undefined) {
                return data;
            }
            return $.param(data);
        }
    })

    .directive('ngSpinnerLoader', ['$rootScope', '$log',
        function($rootScope, $log) {
            var responsespin, routechangespin;
            return {
                link: function(scope, element, attrs) {
                    var addLoader = function(val) {
                        element.addClass('loading' + val);
                    }

                    var removeLoader = function(val) {
                        element.removeClass('loading' + val);
                    }

                    $rootScope.$on('$routeChangeStart', function() {
                        addLoader(2);
                    });

                    $rootScope.$on('$routeChangeSuccess', function(a, b) {
                        clearTimeout(responsespin);
                        routechangespin = setTimeout(function() {
                            removeLoader(2);
                        }, 500);

                        var top = b.$$route.originalPath == "/" ? 0 : $('header').height()
                        $("html, body").animate({
                            scrollTop: top
                        }, 200);
                    });
                }
            };
        }
    ])

    .directive('onLastRepeat', function() {
        return function(scope, element, attrs) {
            if (scope.$last) setTimeout(function() {
                scope.$emit('onRepeatLast', element, attrs);
            }, 100);
        };
    })

    .run(function($rootScope, $http, $location, $window, $log) {
        $log.info('Running CIEB_webApp');

        $rootScope.$on('$routeChangeStart', function(next, current) {
            $rootScope.pageclass = (current.$$route) ? current.$$route.originalPath.split('/')[1] : 'home';
            $rootScope.pageclass = $rootScope.pageclass || 'home';
            $('.navbar-collapse.max-mobile-navbar.collapse').removeClass('in');
        });

        $rootScope.$on('$routeChangeSuccess', function(next, current) {

        });

        $log.debug("$rootScope", $rootScope);
    })

    .filter('unsafe', function($sce) {
        return $sce.trustAsHtml;
    })

    .filter('moment_from_now', function() {
        return function(time) {
            return moment(new Date(time)).fromNow();
        }
    })
})(window, document, window.angular, window.moment, window.jQuery);
