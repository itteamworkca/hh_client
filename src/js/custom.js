(function(window, document, $, Modernizr, undefined) {

	// INSPINIA Landing Page Custom scripts
	$(document).ready(function () {

	    // Highlight the top nav as scrolling
	    $('body').scrollspy({
	        target: '.navbar-fixed-top',
	        offset: 80
	    })

	    // Page scrolling feature
	    $('a.page-scroll').bind('click', function(event) {
	        event.preventDefault();
	        var link = $(this);
	        $('html, body').stop().animate({
	            scrollTop: $(link.attr('href')).offset().top - 70
	        }, 500);
	    });

	});

	// Activate WOW.js plugin for animation on scrol
	//new WOW().init();

})(window, document, window.jQuery, window.Modernizr);
