(function() {

    'use strict';

    angular
        .module('webApp')
        .factory('SearchResource', SearchResource);

    function SearchResource($resource, API) {
        return $resource(API.ENDPOINT + '/json/search.json?:searchString');
    }

})();
