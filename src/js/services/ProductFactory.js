(function() {
    'use strict';

    angular
        .module('SynthesizeApp')
        .factory('ProductResource', ProductResource);

    function ProductResource($resource) {
        return $resource('/api/products/:page', {'page': 1}, {
            highlight: {
                method: 'GET',
                url: '/api/products/highlight',
                isArray: true,
                params: {
                    'page': null
                }
            },
            filters: {
                method: 'GET',
                url: '/api/products/filters',
                params: {
                    'page': null
                }
            },
            canonical: {
                method: 'GET',
                url: '/api/products/:canonical',
                params: {
                    'page': null
                }
            }
        });
    }
})();
