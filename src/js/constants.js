'use strict';

/**
 * Angular Module
 */
angular.module('webApp')

/**
 * Web API constants
 */
.constant('API', {
    ENDPOINT: 'http://localhost:8083',
    TAG: function() {
        return {
            method: 'GET',
            url: this.ENDPOINT + '/eep/tags?nome=',
        }
    },
    FILTROS: function() {
        return {
            method: 'GET',
            url: this.ENDPOINT + '/eep/filtros',
        }
    },
    EMPRESA: function() {
        return {
            method: 'POST',
            url: this.ENDPOINT + '/eep',
        }
    },
    UPLOAD: function() {
        return {
            method: 'POST',
            url: this.ENDPOINT + '/eep/upload',
            autoUpload: true
        }
    }
})

.constant('CEP', {
    ENDPOINT: 'http://172.16.6.83:3000/'
})

.constant('FILE_UPLOAD', {
    IMAGE_FILTER: {
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    },
    DOCUMENT_FILTER: {
        name: 'documentFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|pdf|'.indexOf(type) !== -1;
        }
    }
})
